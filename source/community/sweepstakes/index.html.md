---
layout: markdown_page
title: "GitLab Sweepstakes"
---
## Current and previous giveaways

- [2018 Global Developer Survey Sweepstakes](/community/sweepstakes/2018-developer-survey/)
- [GitLab UX Research Amazon Gift Card Giveaway](/community/sweepstakes/40-dollar-amazon-gift-card/)
- [Content Hack Day April 2018](/community/sweepstakes/content-hack-day/)
- [GitLab Original Shirt Giveaway](/community/sweepstakes/gitlab-original-tee/)