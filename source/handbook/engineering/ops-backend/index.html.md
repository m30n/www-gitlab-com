---
layout: markdown_page
title: "Ops Backend Department"
---

## On this page
{:.no_toc}

- TOC
{:toc}

[Dalia Havens](https://about.gitlab.com/company/team/#dhavens) is the Director for Ops Backend. Feel free to check out her [README](https://about.gitlab.com/handbook/engineering/ops-backend/director/).
## Teams

There are a number of teams within the Ops Backend group:

* [Verify](/handbook/engineering/ops-backend/verify/)
* [Release](/handbook/engineering/ops-backend/release/)
* [Configure](/handbook/engineering/ops-backend/configure/)
* [Monitor](/handbook/engineering/ops-backend/monitoring/)
* [Secure](/handbook/engineering/ops-backend/secure/)

Each team has a different focus on what issues to work on for each
release. The following information is not meant to be a set of hard-and-fast
rules, but as a guideline as to what team decides can best improve certain
areas of GitLab.

APIs should be shared responsibility between all teams within the
Backend group.

There is a backend group call every Tuesday, before the company call. You should
have been invited when you joined; if not, ask your team lead!

[Find the product manager mapping to engineering teams in the product handbook](/handbook/product)
